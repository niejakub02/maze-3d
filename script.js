import * as THREE from "./build/three.module.js";
import { OrbitControls } from "./build/OrbitControls.js";

let labirynth = [];
let key = [];
let length = 10;
let width = 8;
let exit, entrance;
let moveFront = true;
let moveBack = true;
let Maze = new MazeBuilder(width, length);
const direction = new THREE.Vector3();
let raycasterFront = new THREE.Raycaster();
let raycasterBack = new THREE.Raycaster();
const pointLight = new THREE.PointLight(0xffffff);
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  100000
);

const container = document.querySelector(".container");
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
container.appendChild(renderer.domElement);
// const controls = new OrbitControls(camera, renderer.domElement);
const textureLoader = new THREE.TextureLoader();
var texture1 = textureLoader.load("textures/wall.jpg");
var texture2 = textureLoader.load("textures/floor.jpg");
texture2.wrapS = THREE.RepeatWrapping;
texture2.wrapT = THREE.RepeatWrapping;
texture2.repeat.set(64, 64);
init(texture1, texture2);
animate();

function init(texture1, texture2) {
  const geometry = new THREE.BoxGeometry(20, 20, 20);
  const plane = new THREE.PlaneGeometry(500, 500);

  const material = new THREE.MeshStandardMaterial({
    color: "gray",
    side: THREE.FrontSide,
    map: texture1,
  });

  const material2 = new THREE.MeshStandardMaterial({
    color: "red",
    side: THREE.FrontSide,
    wireframe: true,
  });

  const material3 = new THREE.MeshStandardMaterial({
    color: "gray",
    side: THREE.BackSide,
    map: texture2,
  });

  for (let [i, r] of Maze.maze.entries()) {
    for (let [j, m] of r.entries()) {
      if (m == "wall") {
        labirynth.push(new THREE.Mesh(geometry, material));
      }

      if (m[0] == "door") {
        if (i == 0) {
          labirynth.push(new THREE.Mesh(geometry, material2));
          exit = j;
        } else {
          labirynth.push(new THREE.Mesh(geometry, material));
          entrance = i * 17 + j;
        }

        console.log(`${exit}, ${entrance}`);
      }

      if (m == "") {
        labirynth.push("");
      }
    }
  }


  console.log(labirynth);

  let j = 0;
  for (let i = 0; i < 357; i++) {
    if (i != 0 && i % 17 == 0) j++;
    if (labirynth[i] instanceof THREE.Mesh) {
      if (i == entrance) {
        camera.position.x = (i % 17) * 20;
        camera.position.z = j * 20;
        pointLight.position.x = (i % 17) * 20;
        pointLight.position.z = j * 20;

        scene.add(labirynth[i]);
        labirynth[i].position.x = (i % 17) * 20;
        labirynth[i].position.z = (j + 1) * 20;
      } else {
        scene.add(labirynth[i]);
        labirynth[i].position.x = (i % 17) * 20;
        labirynth[i].position.z = j * 20;
      }
    }
  }

  const floor = new THREE.Mesh(plane, material3);
  scene.add(floor);
  floor.position.x = (width / 2) * 40;
  floor.position.y = -10;
  floor.position.z = (length / 2) * 40;
  floor.rotateX(Math.PI / 2);

  // const light = new THREE.AmbientLight(0xffffff, 1);
  // scene.add(light);
  pointLight.intensity = 0.75;
  pointLight.distance = 50;
  scene.add(pointLight);

  document.addEventListener("keydown", function (event) {
    key[event.keyCode] = true;
  });
  document.addEventListener("keyup", function (event) {
    key[event.keyCode] = false;
  });

  renderer.render(scene, camera);
  window.addEventListener("resize", function () {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.render(scene, camera);
  });
  // camera.position.z = 1;
  // controls.update();
}

function move(e) {
  if (key["w".charCodeAt(0)] || key["W".charCodeAt(0)]) {
    if (moveFront) {
      camera.getWorldDirection(direction);
      raycast();
      camera.position.addScaledVector(direction, 0.3);
      pointLight.position.copy(camera.position);
    }
  }

  if (key["s".charCodeAt(0)] || key["S".charCodeAt(0)]) {
    if (moveBack) {
      camera.getWorldDirection(direction);
      raycast();
      camera.position.addScaledVector(direction, -0.3);
      pointLight.position.copy(camera.position);
    }
  }

  if (key["d".charCodeAt(0)] || key["D".charCodeAt(0)]) {
    camera.getWorldDirection(direction);
    raycast();
    camera.rotation.y -= 0.05;
    pointLight.rotation.copy(camera.rotation);
  }

  if (key["a".charCodeAt(0)] || key["A".charCodeAt(0)]) {
    camera.getWorldDirection(direction);
    raycast();
    camera.rotation.y += 0.05;
    pointLight.rotation.copy(camera.rotation);
  }
}

function animate() {
  requestAnimationFrame(animate);

  // controls.update();
  move();

  renderer.render(scene, camera);
}

function raycast() {
  let dFront = direction.clone();
  let dBack = direction.clone();
  dBack.applyAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI);

  raycasterFront.set(camera.position, dFront);
  raycasterBack.set(camera.position, dBack);
  let intersectsFront = raycasterFront.intersectObjects(scene.children, true);
  let intersectsBack = raycasterBack.intersectObjects(scene.children, true);

  if (intersectsFront.length > 0) {
    moveFront = prohibitMovement(intersectsFront);
  }

  if (intersectsBack.length > 0) {
    moveBack = prohibitMovement(intersectsBack);
  }
}

function prohibitMovement(intersects) {
  for (let intersect of intersects) {
    if (intersect.object.uuid == labirynth[exit].uuid && intersect.distance < 3)
      return endGame();
    if (intersect.distance < 2) return false;
  }
  return true;
}

function endGame() {
  alert("CONGRATULATIONS, YOU FOUND A WAY OUT");
  return window.location.reload(true);
}
