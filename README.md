### MAZE-3D

***

### DESCRIPTION

Short game created as an final project for 'Basic of computer graphics' classes. It creates labirynth and player's objective is to find a way out. 

### ATRIBUTES

- Maze is generated randomly (look at references),
- It is needed to run local server to play the game,
- Project uses threejs library for creating graphics:
    - Thanks to implemented GUI you can:
        - affect maze's dimensions,
        - choose between two cameras (cam0 - camera used to play the game, cam1 - used to view the maze),
    - Smooth movement,
    - Raycasters to detect collisions,
    - Dark location with dynamic light,
    - Textures and materials interacting with lights,
- By finding a way out you can complete the game (page reload to start a new game).

### REFERENCES

Project is using maze generator NOT created by me. You can find used algorithm in the link bellow:
https://www.the-art-of-web.com/javascript/maze-generator/
